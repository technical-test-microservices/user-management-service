package com.library.usermanagementservice.service;

import com.library.usermanagementservice.domain.Student;
import com.library.usermanagementservice.dto.FindOneResponse;
import com.library.usermanagementservice.exception.CustomBadRequestException;
import com.library.usermanagementservice.exception.CustomNotFoundException;
import com.library.usermanagementservice.exception.GeneralBodyResponse;
import com.library.usermanagementservice.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public GeneralBodyResponse createStudents(List<Student> students) {
        Date currentDate = new Date();
        for (Student student : students) {
            Optional<Student> existingStudent = studentRepository.findByNisn(student.getNisn());
            if (existingStudent.isPresent()) {
                throw new CustomBadRequestException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "NISN " + student.getNisn() + " already exist.");
            }
            student.setCreatedAt(currentDate);
            student.setActive(true);
        }
        List<Student> createdStudents = studentRepository.saveAll(students);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Students created successfully", createdStudents);
    }

    public GeneralBodyResponse updateStudents(List<Student> students) {
        List<Student> updatedStudents = new ArrayList<>();
        Date currentDate = new Date();
        for (Student student : students) {
            Optional<Student> existingStudent = studentRepository.findById(student.getId());
            if (existingStudent.isEmpty()) {
                throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Student with ID " + student.getId() + " not found.");
            }
            Student existing = existingStudent.get();
            existing.setName(student.getName());
            existing.setActive(student.isActive());
            existing.setUpdatedAt(currentDate);
            updatedStudents.add(existing);
        }
        List<Student> savedStudents = studentRepository.saveAll(updatedStudents);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Students updated successfully", savedStudents);
    }

    public GeneralBodyResponse findAllStudents() {
        List<Student> students = studentRepository.findAll();
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully get all student", students);
    }

    public GeneralBodyResponse findStudentById(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Id  " + id + " not found."));
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully get by id student", student);
    }

    public GeneralBodyResponse findStudentByNisn(String nisn){
        Student student = studentRepository.findByNisnAndActive(nisn,true)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "NISN  " + nisn + " not found."));

        FindOneResponse response = new FindOneResponse();
        response.setNisn(student.getNisn());
        response.setName(student.getName());
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully get by id student", response);
    }

    public GeneralBodyResponse deleteStudent(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Id " + id + " not found."));
        student.setActive(false);
        student.setUpdatedAt(new Date());
        Student deletedStudent = studentRepository.save(student);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Student deleted successfully", deletedStudent);
    }
}
