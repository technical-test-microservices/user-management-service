package com.library.usermanagementservice.controller;

import com.library.usermanagementservice.domain.Student;
import com.library.usermanagementservice.exception.GeneralBodyResponse;
import com.library.usermanagementservice.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/students")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> createStudents(@RequestBody List<Student> students) {
        GeneralBodyResponse response = studentService.createStudents(students);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @PutMapping("/update")
    public ResponseEntity<GeneralBodyResponse> updateStudents(@RequestBody List<Student> students) {
        GeneralBodyResponse response = studentService.updateStudents(students);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/findAll")
    public ResponseEntity<GeneralBodyResponse> findAllStudents() {
        GeneralBodyResponse response = studentService.findAllStudents();
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GeneralBodyResponse> findStudentById(@PathVariable Long id) {
        GeneralBodyResponse response = studentService.findStudentById(id);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/findByNisn")
    public ResponseEntity<GeneralBodyResponse> findStudentByNisn(@RequestParam String nisn) {
        GeneralBodyResponse response = studentService.findStudentByNisn(nisn);
        return ResponseEntity
                .ok()
                .body(response);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<GeneralBodyResponse> deleteStudent(@PathVariable Long id) {
        GeneralBodyResponse response = studentService.deleteStudent(id);
        return ResponseEntity
                .ok()
                .body(response);
    }
}
