package com.library.usermanagementservice.repository;

import com.library.usermanagementservice.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    Optional <Student> findByNisn(String nisn);

    Optional<Student> findByNisnAndActive(String nisn,boolean active);
}
