package com.library.usermanagementservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FindOneResponse {

    private String name;
    private String nisn;
}
